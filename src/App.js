import { useState, useEffect, useContext} from 'react';
import { UserProvider } from './UserContext';
import {BrowserRouter as Router} from 'react-router-dom';
import {Routes, Route} from 'react-router-dom'; 
import AdminOrderProduct from './pages/AdminOrderPage';
import AdminProducts from './pages/AdminProduct'
import Orders from './pages/Orders'
import Profile from './pages/Profile'
import Cart from './pages/Cart'
import ProductView from './pages/ProductView';
import  Logout from './pages/Logout';
import Product from './pages/Products';
import Register from './pages/Register'
import Home from './pages/Home';
import Login from './pages/Login'
import OnSale from './components/OnSale'
import Banner from './components/Banner';
import { Fragment} from 'react'
import { Container } from 'react-bootstrap'
import AppNavBar from './components/AppNavBar';
import './App.css';

function App() {
  
   // State hook for the user state that's defined here for a global scope.
  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  // This function is for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch('https://fast-badlands-56837.herokuapp.com/users/details', {
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {


        if(typeof data._id !== "undefined") {

          setUser({
            id:data._id,
            isAdmin: data.isAdmin
          })

        } else {
          setUser({
            id: null,
            isAdmin: null
          })
        }
    })



  }, [])

  return (
    
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
   <AppNavBar/>
    <Container>
     
       <Routes>
        
          <Route path="/login" element={<Login/>} />
          <Route path="/logout" element={<Logout/>} />
          <Route path="/" element={<Home/>} />
          <Route path="/products" element={<Product/>} />
          <Route path="/products/:productId" element={<ProductView/>} />
          <Route path="/register" element={<Register/>} />
          <Route path="/cart" element={<Cart/>} />
          {/*<Route path="*" element={<NotFound/>} />*/}
          <Route path="/profile" element={<Profile/>} />
          <Route path="/orders" element={<Orders/>} />
          <Route path="/adminProduct" element={<AdminProducts/>} />
          <Route path="/adminOrderProduct" element={<AdminOrderProduct/>} />

        </Routes>
    </Container>
    </Router>
  </UserProvider>
  )
}

export default App;
