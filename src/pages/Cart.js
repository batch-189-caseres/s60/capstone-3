import { useNavigate, Navigate  } from 'react-router-dom';
import Swal from 'sweetalert2';
import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Stack from 'react-bootstrap/Stack';
import { Button, Container, Card } from 'react-bootstrap';
import UserContext from '../UserContext';
import CartCard from '../components/CartCard';
import {Fragment, useEffect, useState, useContext} from 'react';
import { Row } from 'react-bootstrap';
export default function Cart() {
	const navigate = useNavigate() 
	const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
	const [total, setTotal] = useState('0')
	const { user,setUser} = useContext(UserContext)
	const [cart, setCart] = useState([])
	const [empty, setEmpty] = useState()
	const token = localStorage.getItem('token')

	



	const error = ()  => {
		Swal.fire({
				title: "Success",
				icon: "success",
				text: "Check your balance"
			})
	}
	const retrieveUserDetails = () => {

		fetch('https://fast-badlands-56837.herokuapp.com/users/details', {
			headers: {
				Authorization: `Bearer ${token}` 
			}

		})
		.then(res => res.json())
		.then(data => {

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})

			if(data.isAdmin === true){
				return error()
			}
		})

	}
	const emptycart = () => {
		fetch('https://fast-badlands-56837.herokuapp.com/users/emptyCart', {
			method: 'DELETE',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setCart([])
		})
	}

	const payment = async (data) => {
		console.log(data)
		for(let i = 0; i < data.length; i++){
			fetch(`https://fast-badlands-56837.herokuapp.com/users/${data[i].productId}/checkout`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}` 
			},
			body: JSON.stringify({
				quantity: data[i].quantity,
				totalAmount: data[i].totalAmount
				})
		})
			Swal.fire({
				title: "Success",
				icon: "success",
				text: "Check your balance"
			})
	}
			
		
	}

	const checkout = async () => {
		fetch('https://fast-badlands-56837.herokuapp.com/users/getTotal', {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${token}` 
			}

		})
		.then(update => update.json())
		.then(updatedSubTotal => {
			fetch('https://fast-badlands-56837.herokuapp.com/users/getBalance', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(item => item.json())
		.then(balance => {
			console.log(balance)
			if( balance == false){
				Swal.fire({
				title: "Insuficient Balance",
				icon: "error",
				text: "Check your balance"
			})
		} 
		else { 
			fetch('https://fast-badlands-56837.herokuapp.com/users/checkout', {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${token}` 
			}
		})
		.then(res => res.json())
		.then(data => {
			for(let i = 0; i < data.length; i++){
				fetch(`https://fast-badlands-56837.herokuapp.com/users/${data[i].productId}/checkout`, {
					method: 'POST',
					headers: {
			        	Authorization: `Bearer ${token}` 
			         }
			    
		         })
				
	 			
			}
			emptycart()
			navigate("/cart")
			console.log(data)
			payment(data)

		})
		
	}
 })
			
})
}






	useEffect(() => {
		console.log(user)
		fetch('https://fast-badlands-56837.herokuapp.com/users/checkCart', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data == false){
					
					setEmpty(true)
					setCart(<Card.Img  src={require ("../images/cartempty1.png")} />)
					
			} else {
				setEmpty(false)
				setCart(data.map(product => {
				return (
				<CartCard key={product._id} cartProp={product}/>
				)
				}))
			}
			
		})

		fetch('https://fast-badlands-56837.herokuapp.com/users/getTotal', {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(result => {
			setTotal(result)
		})





	}, [])




	return (

		(user.isAdmin === true ) ?
		<Navigate to="/" />
		:
		
		
			(empty == true ) ?
			<Fragment>
			<Row>
			{cart}
			</Row>
			</Fragment>
			 :
			<Fragment>
			<h1 className="text-center">Cart</h1>
			<Row>
			{cart}
			</Row>
			<Container className="text-center d-grid gap-2">
			<Stack className="py-4" direction="horizontal" gap={3}>
      		<div className="bg-light border"><h3>Total:</h3></div>
      		<div className="bg-light border ms-auto"><h3>Php</h3></div>
      		<div className="vr" />
      		<div className="bg-light border"><h3>₱ {total}</h3></div>
    		</Stack>			
			<Button size="md" className="mb-3"  onClick={handleShow}>Check Out</Button>
			<Modal show={show} onHide={handleClose}>
       		<Modal.Header closeButton>
          	<Modal.Title >Checkout</Modal.Title>
        	</Modal.Header>
        	<Modal.Body>Are sure you want to checkout?</Modal.Body>
        	<Modal.Footer>
          	<Button variant="secondary" onClick={handleClose}>
            Close
          	</Button>
          	<Button variant="primary" onClick={() => checkout()}>
            Checkout
          	</Button>
        	</Modal.Footer>
      		</Modal>
			</Container>
		</Fragment>
	)
}


