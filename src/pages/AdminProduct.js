import { useNavigate, Navigate  } from 'react-router-dom';
import Swal from 'sweetalert2';
import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Stack from 'react-bootstrap/Stack';
import { Button, Container, Form, Card, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import AdminProductCard from '../components/AdminProductCard';
import {Fragment, useEffect, useState, useContext} from 'react';
import { Row } from 'react-bootstrap';
	
export default function AdminProducts({adminProp}) {
     console.log(adminProp)
		 const token = localStorage.getItem('token')	
		 const navigate = useNavigate() 
		 const [getName, setName] = useState("")
    	 const [getCategory, setCategory] = useState("")
    	 const [getPrice, setPrice] = useState("")
    	 const [getImgUrl, setImgUrl] = useState("")
    	 const [getDescription, setDescription] = useState("")
    	 const [getStocks, setStocks] = useState("")
    	 const [getDiscount, setDiscount] = useState()
    	 const [getIsActive, setIsActive] = useState()
    	 const [getOnSale, setOnSale] = useState()
    	 const [getFields, setFields] = useState(false)

		const values = [true];
  		const [fullscreen, setFullscreen] = useState(true);
  		const [show, setShow] = useState(false);
		const [products, setProducts] = useState([])

		const checkFields = () => {
			if(getName !== "" && getCategory !== "" && getPrice !== "" && getImgUrl !== "" && getDescription !== "" && getStocks !== "") 
		{
		setFields(true)
	} else {
		setFields(false)
		return Swal.fire({
				title: "Empty Fields",
				icon: "error"
			})
	}
}
	
		const refresh = () => {
		fetch('https://fast-badlands-56837.herokuapp.com/products/getAllProduct')
		.then(res => res.json())
		.then(data => {
			
			setProducts(data.map(product => {
			return (
			<AdminProductCard key={product._id} productProp={product}/>
			)
		}))		
	})
}

		
		function handleShow(breakpoint) {
    	setFullscreen(breakpoint);
    	setShow(true);
  		}


  		const createNewProduct = async () => {
  			await checkFields()

  			if(getFields == true){
  				fetch(`https://fast-badlands-56837.herokuapp.com/products/`,{
  				method: 'POST',
  				headers: {
  					'Content-Type': 'application/json',
  					Authorization: `Bearer ${localStorage.getItem('token')}`
  				},
  				body: JSON.stringify({
				name: getName,
    			description: getDescription,
    			imgUrl: getImgUrl,
    			category: getCategory,
    			price: getPrice,
   				stocks : getStocks
				})
  			})
  			.then(res => res.json())
  			.then(data => {
  				if(data == true){
  					setImgUrl("")
  					setShow(false)
  					refresh()
  					Swal.fire({
				title: "Successfully Added",
				icon: "success"
			})
  					navigate("/adminProduct")

  				} else {
  					Swal.fire({
				title: "Error ",
				icon: "error"
			})
  				}
  			})
  		} 
  }




		useEffect(() => {
		
		fetch('https://fast-badlands-56837.herokuapp.com/products/getAllProduct')
		.then(res => res.json())
		.then(data => {
			
			setProducts(data.map(product => {
			return (
			<AdminProductCard key={product._id} productProp={product}/>
			)
		}))
			
			
	})
},[])
	

	return (

		<Container>
		<Modal show={show} fullscreen={fullscreen} onHide={() => setShow(false)}>
        <Modal.Header closeButton>
        <Modal.Title>Add New Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        	<Container className="text-center">
        		<Card.Img className="images" variant="left" src={getImgUrl}/>
        	</Container>
        	<Container>
        	<Form>
        	<Row>
            
            <Col  xs={{span:12}} md={{span:8}}>
              <Form.Control
              	className="mt-4"
              	placeholder="Product Name"
                type="text"

                onChange={e => {
					setName(e.target.value)
				}}
                autoFocus
              />
              </Col>
              <Col xs={{span:12}} md={{span:4}}>
              <Form.Control
              	className="mt-4"
              	placeholder="Category"
                type="text"

                onChange={e => {
					setCategory(e.target.value)
				}}
              />
              </Col>
              <Col xs={{span:12}} md={{span:12}}>
              <Form.Control
              className="mt-4"
              placeholder="Description" 
              as="textarea" 
              rows={3} 

              onChange={e => {
					setDescription(e.target.value)
				}} />
              </Col>
        	  <Col xs={{span:12}} md={{span:12}}>
              <Form.Control
              className="mt-4"
              placeholder="Image Url" 
              as="textarea" 
              rows={3} 
 
              onChange={e => {
					setImgUrl(e.target.value)
				}} />
              </Col>
              <Col xs={{span:12}} md={{span:3}}>
              <Form.Control
              	className="mt-4"
              	placeholder="Price"
                type="text"

                onChange={e => {
					setPrice(e.target.value)
				}}
              />
              </Col>
              <Col xs={{span:12}} md={{span:3}}>
              <Form.Control
              	className="mt-4"
              	placeholder="Stocks"
                type="text"
   
                onChange={e => {
					setStocks(e.target.value)
				}}
              />
              </Col>	
              <Col xs={{offset:1,span:2}} md={{offset: 3,span:1}}>
              	<Button className="mt-4" variant="danger" onClick={() => setShow(false)}>Cancel</Button>
              </Col>
              <Col xs={{offset: 2,span:7}} md={{offset:0,span:2}}>
              	<Button className="mt-4" onClick={() => createNewProduct()}>Save Changes</Button>
              </Col>	
            </Row>
          </Form>
          </Container>
        </Modal.Body>
      	</Modal>
		<h1 className=" text-center py-4">Products</h1>
		{values.map((v) => (
        <Button className="me-2 mb-2" onClick={() => handleShow(v)}>
          Add New Product
        </Button>
         ))}
				<Button className="me-2 mb-2"  onClick={() => refresh()}>
          Refresh
        </Button>
		{products}
		</Container>


		)
}
