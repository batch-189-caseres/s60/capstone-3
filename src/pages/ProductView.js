import Swal from 'sweetalert2';
import { useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col} from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext'
export default function ProductView() {

	const { user } = useContext(UserContext)
	// Allo us to gain access to methods that will allow us to redirect a user to a different page
	// after enrolling a course
	const navigate = useNavigate() //before the version 6 it is called useHistory

	// The "useParams" 
	const {productId} = useParams()
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [img, setImgUrl] = useState("")
	const [quantity] = useState(1)


	const addToCart = (productId) => {
		fetch(`https://fast-badlands-56837.herokuapp.com/users/${productId}/addToCart`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				quantity: quantity,
				totalAmount: price
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true) {
				Swal.fire({
					title: "Added to cart",
					icon: "success"
				})

				navigate("/products")
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again!"
				})
			}
		})
	}

	useEffect(() => {

		fetch(`https://fast-badlands-56837.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setImgUrl(data.imgUrl)
		})
	}, [productId])

	return (

		<Container className="mt-3">
			<Row>
		    {/*span6 take 6 column and offset 3 left right space column*/}
				<Col lg={{span:6, offset:3}}>
					<Card>
					    <Card.Body className="text-center">
					    	<Card.Img variant="top" src={img}/>
					        <h1>{name}</h1>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					        <Card.Text>Qty: {quantity}</Card.Text>
					        

					        {
					        	user.id !== null ?
					        		<Button variant="primary" onClick={() => addToCart(productId)}>Add to Cart</Button>
					        	:
					        	
					        	<Link className="btn btn-danger" to="/login">Login in to add to cart</Link>	
					        }

					    </Card.Body>
					</Card>
				 </Col>
			</Row>
		</Container>


		)



}