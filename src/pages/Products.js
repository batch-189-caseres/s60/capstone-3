import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import {React, useState, useContext,Fragment, useEffect } from "react";
import OnSale from '../components/OnSale'
import ProductCard from '../components/ProductCard'
import { Row } from 'react-bootstrap';
export default function Product() {

	
	const [product, setProduct] = useState([])
	const [onSale, setOnSale] = useState([])

	useEffect(() => {

		fetch('https://fast-badlands-56837.herokuapp.com/products/')
		.then(res => res.json())
		.then(data => {

			setProduct(data.map(product => {
		return (
				<ProductCard key={product.id} productProp={product}/>
			)
		}))
		})

		fetch('https://fast-badlands-56837.herokuapp.com/products/getAllOnSale')
		.then(res => res.json())
		.then(data => {

			setOnSale(data.map(product => {
		return (
				<OnSale key={product.id} productProp={product}/>
			)
		}))
		})
	}, [])

	


	return (
		
		<Fragment>
			<Row>
			<h1 className="py-3">AMAZING DEALS</h1>
			{onSale}
			</Row>
			<h1>Products</h1>
			<Row>
			{product}
			</Row>
		</Fragment>
		
		
		)
}