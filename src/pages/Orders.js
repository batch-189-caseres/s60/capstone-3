import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import {React, useState, useContext,Fragment, useEffect } from "react";
import OnSale from '../components/OnSale'
import OrderCard from '../components/OrderCard'
import { Row } from 'react-bootstrap';


export default function Order() {

  const [user, setUser] = useState()
  const [isAdmin, setIsAdmin] = useState()
  const [order, setOrder] = useState([])
  const token = localStorage.getItem('token')


  const retreiveOrders = (user) => {
     fetch(`https://fast-badlands-56837.herokuapp.com/users/${user}/getOrder`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}` 
      }
    })
    .then(res => res.json())
    .then(data => {
      if(data == false){
        return (
            <h1>Empty daw</h1>
          )
      } else {
        setOrder(data.map(orders => {
        return (
        <OrderCard key={orders.id} orderProp={orders}/>
      )
    }))
      }
    })
  } 



  useEffect(() => {
    
    fetch(`https://fast-badlands-56837.herokuapp.com/users/details`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response => response.json())
    .then(result => {
      setIsAdmin(result.isAdmin)
      fetch(`https://fast-badlands-56837.herokuapp.com/admin/`)
    .then(res => res.json())
    .then(data => {
      console.log(data[0].userOrder)
      if(data[0].userOrder != 1 ){
        retreiveOrders(data[0].userOrder)
      } else {
        fetch(`https://fast-badlands-56837.herokuapp.com/users/details`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(respond => respond.json())
    .then(request => {
      setUser(request._id)
      retreiveOrders(user)
      
    })
      }
      
    })
    })

    
  
     
  
  }, [])

  return(
      (setOrder == false ) ?
      <h1>wla</h1>
      :
      <>
      <h1 className=" text-center">Orders</h1>
      {order}
      </>
    )
}