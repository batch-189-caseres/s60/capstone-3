import Banner from '../components/Banner';
import OnSale from '../components/OnSale';
import {Fragment, useState, useEffect} from 'react';
import {Row, Col} from 'react-bootstrap';

export default function Home() {
const [onSale, setOnSale] = useState([])
useEffect(() => {

		

		fetch('https://fast-badlands-56837.herokuapp.com/products/getAllOnSale')
		.then(res => res.json())
		.then(data => {

			setOnSale(data.map(product => {
		return (
				<OnSale key={product.id} productProp={product}/>
			)
		}))
		})
	}, [])


	return(

		<Fragment>
			<Banner/>
			<Row className="py-3">
			<Col xs={{span:9}} md={{span:9}} lg={{span:4}}>
				<h1>AMAZING DEALS </h1>
			</Col>
			</Row>		
			<Row>
			{onSale}
			</Row>
		</Fragment>

		)
}