import { useNavigate, Navigate  } from 'react-router-dom';
import Swal from 'sweetalert2';
import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Stack from 'react-bootstrap/Stack';
import { Button, Container } from 'react-bootstrap';
import UserContext from '../UserContext';
import AdminOrderCard from '../components/AdminOrderCard';
import {Fragment, useEffect, useState, useContext} from 'react';
import { Row } from 'react-bootstrap';
	
export default function AdminOrder() {
		const token = localStorage.getItem('token')
		const [order, setOrder] = useState([])



		useEffect(() => {
		
		fetch('https://fast-badlands-56837.herokuapp.com/users/getAllOrder', {
			headers: {
    			"Content-Type": "application/json",
				Authorization: `Bearer ${token}` 
    		}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setOrder(data.map(product => {
				console.log(product)
			return (
			<AdminOrderCard key={product._id} orderProp={product}/>
			)
		}))
})
},[])
	

	return (
		<Container>
		<h1 className=" text-center py-4">Users</h1>
		{order}
		</Container>


		)
}
