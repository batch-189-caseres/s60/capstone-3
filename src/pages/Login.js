import { Navigate } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import {Form, Button}  from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Container, Col, Row } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
export default function Login() {

const { user,setUser} = useContext(UserContext)
const [email, setEmail] = useState("");
const [password, setPassword] = useState("");


const [isActive, setIsActive] = useState(false);



useEffect(() => {

	if(email !== "" && password !== ""){
		setIsActive(true)
	} else {
		setIsActive(false)
	}
}, [email, password])

function loginUser(e) {
	e.preventDefault()
	
	localStorage.setItem("email", email)
	setEmail("")
	setPassword("")

	fetch('https://fast-badlands-56837.herokuapp.com/users/login', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			email: email,
			password: password
		})
	})
	.then(res => res.json())
	.then(data => {

		if(typeof data.access !== "undefined") {
			localStorage.setItem('token', data.access)
			retrieveUserDetails(data.access)

			Swal.fire({
				title: "Login Successful",
				icon: "success",
				text: "You are now logged in!"
			})
		} else {

			Swal.fire({
				title: "Authentication Failed",
				icon: "error",
				text: "Wrong email or password"
			})
		}
	})

	const retrieveUserDetails = (token) => {

		fetch('https://fast-badlands-56837.herokuapp.com/users/details', {
			headers: {
				Authorization: `Bearer ${token}` 
			}

		})
		.then(res => res.json())
		.then(data => {

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})

	}

	setEmail("")
	setPassword("")

}

return(
	(user.id !== null ) ?
		<Navigate to="/" />
		:
	<Container>
	<h3 className=" text-center pt-5">Login</h3>
	<Form onSubmit={(e)=> loginUser(e)}>
      <Form.Group as={Row} className="mb-3" controlId="formPlaintextEmail">
        <Form.Label column sm="2" md="1">
          Email
        </Form.Label>
        <Col sm="10" >
          <Form.Control type="email" 
				placeholder="Enter email"
				value={email} 
				onChange={e => {
					setEmail(e.target.value)
				}}
				required />
        </Col>
      </Form.Group>

      <Form.Group as={Row} className="mb-3" controlId="formPlaintextPassword">
        <Form.Label column sm="2" md="1">
          Password
        </Form.Label>
        <Col sm="10" >
          <Form.Control type="password" 
				placeholder="Password" 
				value={password}
				onChange={e => {
					setPassword(e.target.value)
				}}
				required />
        </Col>
      </Form.Group>
	
	{
		isActive ?
			<Container className="text-center pt-3">
			<Button variant="primary" type="submit" id="submitBtn">
	   		Submit
			</Button>
			</Container>
			:
			<Container className="text-center pt-3">			
			<Button variant="danger" type="submit" id="submitBtn" disabled>
			Submit
			</Button>
			</Container>
	}
	</Form>
	<Container className="text-center pt-3">
	<Form.Text className="d-block pb-3" id="passwordHelpBlock" muted>
        Don't have an account yet?
      </Form.Text>
	<Button  as={Link} to="/register">
		Create new account
	</Button>
	</Container>
	</Container>

	)
}
 
    