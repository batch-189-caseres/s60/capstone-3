const productData = [
	{
		id: "wdc001",
		name: "Product 1",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod",
		price: 45000,
		onOffer: true,
		discount: '10%',
		originalPrice: 50000 
	},
	{
		id: "wdc002",
		name: "Product 2",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod",
		price: 36000,
		onOffer: true,
		discount: '10%',
		originalPrice: 40000
	},
	{
		id: "wdc003",
		name: "Product 3",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod",
		price: 27000,
		onOffer: true,
		discount: '10%',
		originalPrice: 30000
	}
]

export default productData;