import Swal from 'sweetalert2';
import AdminProduct from '../pages/AdminProduct';
import UserContext from '../UserContext';
import {React, useState, useContext } from "react";
import {Card, Button, Table, Modal, Form  } from 'react-bootstrap';
import { Link, useNavigate} from 'react-router-dom';
import { Row,Col, Container } from 'react-bootstrap';
import {Fragment} from 'react'
import '../App.css';

export default function AdminProductCard({productProp}) {
	const navigate = useNavigate()
	const { user,setUser} = useContext(UserContext)
	let { name, category, price, _id, imgUrl, description, stocks, discount, isActive, onSale} = productProp;
	
  const token = localStorage.getItem('token')
	const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [getName, setName] = useState(name)
    const [getCategory, setCategory] = useState(category)
    const [getPrice, setPrice] = useState(price)
    const [getImgUrl, setImgUrl] = useState(imgUrl)
    const [getDescription, setDescription] = useState(description)
    const [getStocks, setStocks] = useState(stocks)
    const [getDiscount, setDiscount] = useState(discount)
    
    const [getOnSale, setOnSale] = useState(onSale)
    const [getIsActive, setIsActive] = useState(isActive)

    const [getActivator, setActivator]= useState(false)
    const [getOperator, setOperator] = useState(true)



    const updateProduct = (productId) => {
    	fetch(`https://fast-badlands-56837.herokuapp.com/products/${productId}`, {
    		method: 'PUT',
    		headers: {
    			"Content-Type": "application/json",
				Authorization: `Bearer ${token}` 
    		},
    		body: JSON.stringify({
				name: getName,
				category: getCategory,
				price: getPrice,
				imgUrl: getImgUrl,
				description: getDescription,
				stocks: getStocks
			})
		})
		.then(res => res.json())
		    .then(data => {

		    	handleClose()	
		    	Swal.fire({
					title: "Updated",
					icon: "success"

				})		
    	})
   }

   const removeProduct = (productId) => {
    fetch(`https://fast-badlands-56837.herokuapp.com/products/${productId}/removeProduct`, {
      method: 'DELETE',
      headers: {
          "Content-Type": "application/json",
        Authorization: `Bearer ${token}` 
        }
    })
    .then(res => res.json())
        .then(data => {          
      if(data == true){
        navigate("/adminProduct")
        handleClose()
        Swal.fire({
        title: "Product Removed",
        icon: "success"
      })
        

      } else {
        Swal.fire({
        title: "Error",
        icon: "error"
      })
      } 
      })
   }

   const archiveProduct = (productId) => {
   	fetch(`https://fast-badlands-56837.herokuapp.com/products/${productId}/archive`, {
   		method: 'PUT',
   		headers: {
    			"Content-Type": "application/json",
				Authorization: `Bearer ${token}` 
    		},
    		body: JSON.stringify({
				isActive: getActivator 
		})
   	})
   	.then(res => res.json())
		    .then(data => {
			if(data == false){
				setIsActive(false)
        setActivator(true) 
			} else {
				setIsActive(true)
        setActivator(false)
			}	
    	})

   }

   const setOnSaleProduct = (productId) => {
   	fetch(`https://fast-badlands-56837.herokuapp.com/products/${productId}/setOnsale`, {
   		method: 'PUT',
   		headers: {
    			"Content-Type": "application/json",
				Authorization: `Bearer ${token}` 
    		},
    		body: JSON.stringify({
				onSale: getOperator  
		})
   	})
   	.then(res => res.json())
		    .then(data => {
			if(data == false){
				setOnSale(false)
        setOperator(true)
			} else {
				setOnSale(true)
        setOperator(false)
			}	
    	})

   }
	
	
	return (

			
	<Container>	
	<Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label>Product Name:</Form.Label>
              <Form.Control
                type="text"
                value={getName}
                onChange={e => {
					setName(e.target.value)
				}}
                autoFocus
              />
            </Form.Group>
             <Form.Group className="mb-3">
              <Form.Label>Category</Form.Label>
              <Form.Control
                type="text"
                value={getCategory}
                onChange={e => {
					setCategory(e.target.value)
				}}
                
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
            >
              <Form.Label>Description:</Form.Label>
              <Form.Control as="textarea" rows={3} value={getDescription} onChange={e => {
					setDescription(e.target.value)
				}} />
            </Form.Group>
            <Form.Group
              className="mb-3"
            >
              <Form.Label>Image Url:</Form.Label>
              <Form.Control as="textarea" rows={3} value={getImgUrl} onChange={e => {
					setImgUrl(e.target.value)
				}} />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Price:</Form.Label>
              <Form.Control
                type="text"
                value={getPrice}
                onChange={e => {
					setPrice(e.target.value)
				}}
                
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Stocks:</Form.Label>
              <Form.Control
                type="text"
                value={getStocks}
                onChange={e => {
					setStocks(e.target.value)
				}}
                
              />
            </Form.Group>

          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={() => removeProduct(_id)}>
            Delete Product
          </Button>       
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={() => updateProduct(_id)}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>	
    <Table striped="columns">
      <thead>
        <tr>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      <Row>
        
        <Col xs={{span:12}} md={{span:3}}>
          <td><Card.Img className="images" variant="left" src={getImgUrl}/></td>
        </Col>
        <Col xs={{span:12}} md={{offset:1,span:5}}>
          <td>{getDescription}</td>
        </Col>          
        <Col xs={{span:12}} md={{offset:0,span:4}}>
          <td >{getName}</td>
        </Col>
        <Col xs={{span:12}} md={{offset:0,span:4}}>
          <td>₱ {getPrice}</td>
        </Col>
        <Col xs={{span:12}} md={{offset:0,span:4}}>
          <td>Stocks: {getStocks}</td> 
        </Col>
        <Col xs={{span:12}} md={{offset:0,span:4}}>
          <td>{getCategory}</td>  
        </Col>
        <Col xs={{span:12}} md={{offset:0,span:4}}>
          {	
          	(getIsActive == true)?
          	
          	<td colSpan={1}><Button onClick={() => archiveProduct(_id)} >Active</Button></td>
          	:
          	<td colSpan={1}><Button onClick={() => archiveProduct(_id)} variant="danger">Inactive</Button></td>
          	

          }
          {	
          	(getOnSale == false)?
          	
          	<td colSpan={1}><Button onClick={() => setOnSaleProduct(_id)} variant="danger">Sale</Button></td>
          	:
          	<td colSpan={1}><Button onClick={() => setOnSaleProduct(_id)} >On Sale</Button></td>
          	

          }      
        </Col>
        <tr className="text-center">
          <td colSpan={3}><Button onClick={handleShow}>Update</Button></td>     
        </tr>
        </Row>
      </tbody>
    </Table>
	</Container>			

		)
}