import {Card, Table} from 'react-bootstrap';

export default function OrderCard({orderProp}) {
const { name, category, totalAmount, _id, imgUrl, description, quantity } = orderProp;
let subTotal = totalAmount * quantity

  return (
    <Table striped="columns">
      <thead>
        <tr>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><Card.Img className="images paddingLeft" variant="left" src={imgUrl}/></td>
          <td>{description}</td>
          </tr>
        <tr className="text-center">
          <td >{name}</td>
          <td>{category}</td>  
        </tr>
        <tr className="text-center">
          <td>₱ {totalAmount}</td>
          <td>{quantity}x</td>     
        </tr>
        <tr className="text-center">
          <td colSpan={2}>₱ {subTotal}</td>     
        </tr>
      </tbody>
    </Table>
  );
}

 