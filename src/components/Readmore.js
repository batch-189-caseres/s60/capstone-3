import React, { useState } from "react";
import "../App.css";

const ReadMore = ({ children }) => {
const text = children;
const [isReadMore, setIsReadMore] = useState(true);
const toggleReadMore = () => {
	setIsReadMore(!isReadMore);
};
return (

	(text.length < 35) ?
	<p className="text">
	   {isReadMore ? text.slice(0, 30) : text}
		<span onClick={toggleReadMore} className="read-or-hide">
		{isReadMore ? " " : " "}
	    </span>
	 </p>
	  :
	<p className="text">
	{isReadMore ? text.slice(0, 35) : text}
		<span onClick={toggleReadMore} className="read-or-hide">
		<strong>{isReadMore ? "...read more" : " show less"}</strong>
	    </span>
	</p>
);
};

const Content = ({descriptionProp}) => {
console.log(descriptionProp)
return (
	
	
		<ReadMore>
		{descriptionProp.toLowerCase()}
		</ReadMore>
	
	
);
};

export default Content;
