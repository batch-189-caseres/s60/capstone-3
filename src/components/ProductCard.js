import Readmore from './Readmore.js'; 
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {React, useState, useContext, useEffect } from "react";
import {Card, Button } from 'react-bootstrap';
import { Link, useNavigate} from 'react-router-dom';
import { Col, Container } from 'react-bootstrap';
import {Fragment} from 'react'
import '../App.css';

export default function ProductCard({productProp}) {
	const navigate = useNavigate()
	const { user,setUser} = useContext(UserContext)
	let { name, category, price, _id, imgUrl, description } = productProp;
	const [getDescription, setDescription] = useState ("")
	let quantity = 1
	const token = localStorage.getItem('token')
	// const lowerCase = description.toLowerCase()
	const addToCart = (productId) => {
	
		fetch(`https://fast-badlands-56837.herokuapp.com/users/${productId}/itemCheckCart`, {
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data == true){
				fetch(`https://fast-badlands-56837.herokuapp.com/users/${productId}/addItemQuantity`, {
					method: 'PUT',
					headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${token}`
					}
				})
				.then(res => res.json())
			    .then(result => {
			    	console.log(result)
				if (result === true) {
				Swal.fire({
					title: "Added to cart",
					icon: "success"

				})
				navigate("/products")

				} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again!"
				})
			}
		})
			} else {
				fetch(`https://fast-badlands-56837.herokuapp.com/users/${productId}/addToCart`, {
				method: 'POST',
				headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
				},
				body: JSON.stringify({
				quantity: quantity,
				totalAmount: price
				})
			})
			.then(res => res.json())
			.then(response => {

				if (response === true) {
				Swal.fire({
					title: "Added to cart",
					icon: "success"

				})
				navigate("/products")

				} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again!"
				})
			}
		})
	}
})
}

const details = (id) => {
	navigate(`/products/${id}`)
}

useEffect (() => {
	setDescription(() => { console.log(description)
		return (
		<Readmore descriptionProp = {description}/>
		)
	}
)
}, [])

	return (

			
				
					<Col xs={6} lg={3}>
						<Card className=" text-center" >
					    <Card.Body   style={{textDecoration: 'none', color: 'black'}}>
					    <Card.Img className="images" variant="top" src={imgUrl}  onClick={() => details(_id)}  />
							<h6 className="pt-2 responsiveFont">{name}</h6>
							<p className="responsiveDescription" >{getDescription}</p>							
							{/*<Card.Text>{category}</Card.Text>*/}
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>₱ {price}</Card.Text>
							<Col xs={12} className="text-center">
								<Button variant="primary" size="sm" className="w-100 mb-2"  onClick={() => addToCart(_id)}>Add To Cart</Button>
							</Col>
							{/*<Col >
								<Button variant="secondary"  as={Link} to={`/products/${_id}`}>Details</Button>							
							</Col>*/}
        					
					    </Card.Body>
				        </Card>
					</Col>
				

		)
}

// User Credentials:
//   - Admin User:
//     -- email: admin@email.com
//     -- password: admin123
//   - Dummy Customer
//     -- email: customer@mail.com
//     -- password: customer123