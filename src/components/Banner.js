import {Row, Col} from 'react-bootstrap';
import Carousel from 'react-bootstrap/Carousel';

export default function Banner() {
  return (
    <Row className="pb-5">
       <Col xs={12}>
        <Carousel className="pt-5 carousel">
      <Carousel.Item interval={1000}>
        <img
          className="d-block w-100 "
          src={require ("../images/2.png")}
          alt="First slide"
        />
      </Carousel.Item>
      <Carousel.Item interval={500}>
        <img
          className="d-block w-100 "
          src={require ("../images/1.png")}
          alt="Second slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100 "
          src={require ("../images/3.png")}
          alt="Third slide"
        />
        </Carousel.Item>
        </Carousel>
      </Col>
    </Row>
  );
}



