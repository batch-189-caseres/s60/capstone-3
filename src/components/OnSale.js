import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {React, useState, useContext } from "react";
import {Card, Button } from 'react-bootstrap';
import { Link, useNavigate, Navigate} from 'react-router-dom';
import { Row, Col, Container, ListGroup } from 'react-bootstrap';
import {Fragment} from 'react'
export default function OnSale ({productProp}) {
 let { name, category, price, _id, imgUrl, description } = productProp;
 let discount = 10
 const token = localStorage.getItem('token')
 const navigate = useNavigate() 
let quantity = 1
let discountedPrice = ((discount / 100) * price)
let nowPrice = price - discountedPrice
let fixPrice = nowPrice.toFixed(2)
let savePrice = price - fixPrice
let savedPrice = savePrice.toFixed(2) 
 const addToCart = (productId) => {
	
		fetch(`https://fast-badlands-56837.herokuapp.com/users/${productId}/itemCheckCart`, {
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data == true){
				fetch(`https://fast-badlands-56837.herokuapp.com/users/${productId}/addItemQuantity`, {
					method: 'PUT',
					headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${token}`
					}
				})
				.then(res => res.json())
			    .then(result => {
			    	console.log(result)
				if (result === true) {
				Swal.fire({
					title: "Added to cart",
					icon: "success"

				})
				navigate("/products")

				} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again!"
				})
			}
		})
			} else {
				fetch(`https://fast-badlands-56837.herokuapp.com/users/${productId}/addToCart`, {
				method: 'POST',
				headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
				},
				body: JSON.stringify({
				quantity: quantity,
				totalAmount: price
				})
			})
			.then(res => res.json())
			.then(response => {

				if (response === true) {
				Swal.fire({
					title: "Added to cart",
					icon: "success"

				})
				navigate("/products")

				} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again!"
				})
			}
		})
	}
})
}

	return (



	<Fragment>
		
		
		
			<Col xs={6} lg={3}>
			<Card className=" mx-auto fluid" as={Link} to={`/products/${_id}`} style={{textDecoration: 'none', color: 'black'}} >
				<Card.Img variant="top" className="images" src={imgUrl}/>
				<Card.Body  className="text-center">
					<p className= "responsiveFont">{name}</p>	
					<h4 className="text-center">₱ {fixPrice}</h4>
					<Card.Subtitle><s>₱ {price}</s><h6><i>{discount}% off</i></h6></Card.Subtitle>			
					{/*<h6><p style={{color: "red"}}> save ₱ {savedPrice}</p></h6>	*/}			
				
							<Col xs={12} className="text-center">
							<h6><p style={{color: "red"}}> save ₱ {savedPrice}</p></h6>
							</Col>
					        <Col xs={12} className="text-center">
								<Button variant="primary" size="sm" className="w-100 mb-2"  onClick={() => addToCart(_id)}>Add To Cart</Button>
							</Col>
				</Card.Body>	
			</Card>
		</Col>
	

</Fragment>

)
}