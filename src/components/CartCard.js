import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {React, useState, useContext } from "react";
import {Card, Button, Input } from 'react-bootstrap';
import { Link, useParams, useNavigate } from 'react-router-dom';
import { Col, Container, Form, Row } from 'react-bootstrap';
import {Fragment} from 'react'
import '../App.css';
export default function CartCard({cartProp}) {
	const navigate = useNavigate() 
	const { user,setUser} = useContext(UserContext)
	const { name, category, totalAmount, _id, imgUrl, description, productId, quantity } = cartProp;
	let [Quantity, setQuantity] = useState(quantity);
	let price = totalAmount * Quantity;
	let nowPrice = price.toFixed(2)
	const token = localStorage.getItem('token')
	const removeToCart = (productId, token) => {

		fetch(`https://fast-badlands-56837.herokuapp.com/users/${productId}/removeToCart`, {
			method: 'DELETE',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}` 
			}

		})
		.then(res => res.json())
		.then(data => {
			if (data == true) {
				Swal.fire({
					title: "removed to cart",
					icon: "success"
				})

				
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again!"
				})
			}
		})

	}

	const updateQuantity = (productId, token, Quantity) => {
		fetch(`https://fast-badlands-56837.herokuapp.com/users/${productId}/updatedQuantity`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}` 
			},
			body: JSON.stringify({
				quantity: Quantity
				})
		})
		.then(res => res.json())
		.then(data => {
			if (data == true) {
				Swal.fire({
					title: "Updated Quantity",
					icon: "success"
				})

				// setTimeout(() => {  window.location.reload(false); }, 2000);
				
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again!"
				})
			}
})
}

		return (

			
				
					<Col xs={12} lg={12}>
						<Card className=" text-center">
					    <Card.Body>
					    <Card.Img className="images" variant="left" src={imgUrl}/>
							<h4>{name}</h4>
							<Card.Subtitle>{description}</Card.Subtitle>							
							<Card.Text>{category}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>₱ {nowPrice}</Card.Text>
							<Row className="pb-2">
								<Col xs={{offset:1,span:2}} md={{offset:4,span:1}}>
									<Button variant="primary"
									onClick={e => {
									setQuantity(e.target.value--)
									}}
									value={Quantity}>➖</Button>	
								</Col>
								<Col xs={{offset:1,span:4}} md={{offset:0, span:2}} >
									<Form >
									<Form.Group  >
									<Form.Control 
									className=" text-center"
									type="text" 
									onChange={e => {
									setQuantity(e.target.value)
									}}
									value={Quantity}
									required />
									</Form.Group>
									</Form>
								</Col>
								<Col xs={{span:1}}>
									<Button variant="primary" onClick={e => {
									setQuantity(e.target.value++)
									}}
									value={Quantity}>➕</Button>	
								</Col>
							</Row>
							<Col >
								<Button variant="secondary" onClick={()=> removeToCart(productId, token)}>Remove</Button>							
							</Col>
							<Col xs={{offset: 5, span:2}}>
									<Button variant="primary" className="mt-2"  onClick={()=> updateQuantity(productId, token, Quantity)}>Save</Button>	
							</Col>		
					    </Card.Body>
				        </Card>
					</Col>
				

		)
}
