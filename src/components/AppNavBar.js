import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';
import {useContext, useState, useEffect } from 'react'
import UserContext from '../UserContext';
import {Container, Nav, Navbar, NavDropdown, Button, Modal, Form} from 'react-bootstrap';

export default function AppNavBar() {

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [ balance, setBalance] = useState('0')
  const { user } = useContext(UserContext) 
  const token = localStorage.getItem('token')
  const [ cashIn, setCashIn] = useState()
  let parse = Math.floor(cashIn)
  const addBalance = async (cashIn) => {
    if(cashIn >= 100){
      fetch('https://fast-badlands-56837.herokuapp.com/users/cashIn', {
      method: 'PUT',
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify({
        balance: parse
        })
    })
    .then(res => res.json())
    .then(data => {
    
    setBalance(data)
    handleClose() 
    refreshBalance()
    Swal.fire({
          title: `Php ${parse} Added Balance`,
          icon: "success",
          text: "100 is the minimum Please try again!"
        })     
  })

  } else {
    Swal.fire({
          title: "Invalid Number",
          icon: "error",
          text: "100 is the minimum Please try again!"
        })
  }  
  }

   const refreshBalance = () => {
    fetch('https://fast-badlands-56837.herokuapp.com/users/checkBalance', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
    setBalance(data)      
  })
}

  useEffect(() => {  
    fetch('https://fast-badlands-56837.herokuapp.com/users/checkBalance', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
    setBalance(data)      
  })
},[balance])

  return (

   
    

    <Navbar collapseOnSelect expand="lg" className="Nav-color " variant="dark" sticky="top">
       <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add Balance</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label>Cash In</Form.Label>
              <Form.Control
                type="number"
                onChange={e => {
                  console.log(typeof parse)
                setCashIn(e.target.value)}}
                value={cashIn} 
                autoFocus
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={() => addBalance(cashIn)}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
        <Navbar.Brand className="mx-3" as={Link} to="/">
          <img
              src={require ("../images/BlueLogo12.jpg")}
              width="140"
              // height="105"
              className="fluid"
              alt="BLUE BASKET LOGO"
            /></Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          
            {
              (user.isAdmin === true) ?
            <>
            <Nav className="me-auto">
            <Nav.Link as={Link} to="/adminProduct">Products</Nav.Link>
            <Nav.Link as={Link} to="/adminOrderProduct">Users</Nav.Link>
            <Nav.Link href="#deets"></Nav.Link>
            </Nav>
            </>
            :
            <Nav className="me-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/products">Products</Nav.Link>
            <Nav.Link  as={Link} to="/">Contact Us</Nav.Link>
            </Nav>
            
             }
          
             <Nav className="me-5">
            
            
              {

                (user.id !== null && user.isAdmin === false) ?
              <>  
              <Nav.Link className="py-3" onClick={handleShow} >Balance: ₱ {balance.toFixed(2)}</Nav.Link>
              <NavDropdown className="py-2" title="Profile" id="collasible-nav-dropdown">
              <NavDropdown.Item as={Link} to="/profile">Profile</NavDropdown.Item>
              <NavDropdown.Item as={Link} to="/orders">
                Orders
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Settings</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item as={Link} to="/logout">
                Log Out
              </NavDropdown.Item>
              </NavDropdown>
              <Nav.Link  as={Link} to="/cart">
                   <img
              src={require ("../images/Cart.jpg")}
              width="30"
              alt="Cart"
            />
                   </Nav.Link>
              </>
                  :
                  (user.isAdmin === true)?                
                  <>
                    <NavDropdown className="py-2" title="Profile" id="collasible-nav-dropdown">
                    <NavDropdown.Item as={Link} to="/profile">Profile</NavDropdown.Item>
                    <NavDropdown.Item as={Link} to="/orders">
                    Orders
                    </NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.3">Settings</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item as={Link} to="/logout">
                    Log Out
                    </NavDropdown.Item>
                    </NavDropdown>
                  </>
                  :
                  <>
                   <Nav.Link  as={Link} to="/login">
                   Login / Sign up 
                   </Nav.Link>
                  </>
            }
          </Nav>
        </Navbar.Collapse>
      
    </Navbar>
  );
}

