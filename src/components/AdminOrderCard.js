import {Fragment, useEffect, useState, useContext} from 'react';
import {Card, Table, Button} from 'react-bootstrap';
import {Link, useNavigate} from 'react-router-dom';
import Orders from '../pages/Orders';

export default function AdminOrderCard({orderProp}) {
  const navigate = useNavigate() 
  const [orders, setOrder] = useState('')
const { id, email, order } = orderProp;
const token = localStorage.getItem('token')
const [activator, setActivator] = useState(true)
const [deactivator, setDeactivator] = useState(false)
        
const sendId = (id) => {
  fetch(`https://fast-badlands-56837.herokuapp.com/admin/${id}/`, {
    method: 'POST'
  })
     navigate("/orders")
}  

const setAdmin = (id) => {
  fetch(`https://fast-badlands-56837.herokuapp.com/users/${id}/admin`, {
    method: 'PUT',
    headers: {
      "Content-Type": "application/json",
        Authorization: `Bearer ${token}` 
    },
    body: JSON.stringify({
        isAdmin: activator
        })
  }).then(res => res.json())
    .then(data => {
       if(data == true){
        setActivator(false)
        setDeactivator(true)
       } else {
        setActivator(true)
        setDeactivator(false)
       }
    })
} 






  return (
    <Table striped="columns">
      <thead>
      </thead>
      <tbody className="text-center">
        <tr>
          <td>{id}</td>
          
        </tr>
        <tr>
          <td >{email}</td>
        </tr>
        {
          (deactivator == true) ?
        <tr>          
          <td ><Button  onClick={() => setAdmin(id)}>Admin</Button></td>
        </tr> 
          :
        <tr>
          <td ><Button  onClick={() => setAdmin(id)} variant="danger">Admin</Button></td>
        </tr>
        }
        <tr>
          <td ><Button  onClick={() => sendId(id)}>View Orders</Button></td>
        </tr>
      </tbody>
    </Table>
  );
}
